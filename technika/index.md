---
title: Technika
layout: page
permalink: /technika
lang: cs
---
Audiovizuální centrum Silicon Hill je vybaveno zvukovou, světelnou a video technikou.
K dispozici je zvuková technika pro live produkci i studiovou činnost. Videotechnika je dostupná v rozlišení až UHD. 
Tato technika byla pořízena především díky klubu Silicon Hill.

## Mikrofony:

* 2x Neumann TLM 102
* 2x Neumann KM 184
* MXL R-77
* sE Electronics 2200T
* AKG C214
* Studio Projects C-3
* Rode NT-1A
* Rode NTG3 + Rode BLIMB II
* 3x Audio Technica AE 3000
* Shure Beta 91A
* 2x Sennheiser e906
* Sennheiser MD421 mkII
* 2x Studio Projects C-4
* 2x Oktava MK 012 MSP6
* Beyerdynamic OPUS Drum Set:
    * 4x OPUS 88
    * 1x OPUS 99
* 4x Beyerdynamic  TG V71d
* 3x AKG D5
* 3x Shure SM58
* 3x Shure SM57
* Apex 565
* AKG D112
* 2x Shure SM48
* 2x Soundking EH002



## Bezdrátové sety:

* 3x DPA d:fine
* 2x Sennheiser EM 500 G3 + SKM 500-945 G3

* Sennheiser SK 500 G3 G-Band


* Sennheiser EW 100 ENG G3 G-Band
* 2x Sennheiser EM 100 G2 + SK 100 G2 +

* 2x Sennheiser ME 4
* Sennheiser ME 3
* Rode HS1-P


* Sennheiser EW100 G2 + SKM 100 G2



## Studiová technika:

* Předzesilovače
* TL Audio Ivory 5051 MKII
* 2x Studio Projects VTB1


* Zvuková karta a převodníky
    * MOTU 828 MK III
    * M-Audio Profire 2626
    * Steinberg UR44
    * Behringer ADA8200
    * Behringer ADA8000


* Studiové monitory
    * Adam Audio A8X
    * YAMAHA HSP


* Sluchátka
    * 4x Sennheiser HD215
    * Beyerdynamic DT770 Pro


* Sluchátkový distributor
    * Behringer HA8000



## Live technika:

* Reproduktory
    * 2x EV ETX12p
    * 2x EV ZXA1-90B
    * 4x EV ELX 112
    * 2x RCF ART 712 mk II
    * 2x JBL MRX515
    * 4x Valeo reprobox + reproduktor RCF L18P400
    * 2x SPL Acoustics S215
    * 4x LD Systems LDEB 152
    * 4x Yamaha S115-V


* Zesilovače
    * Dynacord L3600FD
    * QSC PL 224
    * 2x Sanway FP10000Q
    * 2x Tapco J1400


* DSP
    * Ashly Protea 3.6SP


* Mixážní pulty a kontrolery
    * Behringer X32
    * Behringer X32 Rack
    * Behringer X-Touch
    * Allen &amp; Heath ZED 428
    * Behringer Xenyx 1832USB
    * Behringer Eurorack UB1002
    * Behringer Eurorack UB1202


* Stageboxy
    * Midas DL32


* Efektový rack a EQ
    * Lexicon MX200
    * dbx 266XL
    * dbx 166XL
    * dbx 1231
    * 2x Behringer FBQ1502


* Záznamová zařízení
    * Zoom H5
    * M-Audio MicroTrack II



## Světelný park:

* Světla
    * 4x Stairville MH-X50 LED Spot moving head
    * 4x Stairville MH-z720 Wash
    * 16x Stairville LED PAR 64 CX3 RGBW 18x8W
    * 12x Stairville LED PAR 64 10mm Floor
    * 2x Stairville LED PAR 64 10mm UV


* Stojany
    * American DJ Light bridge one - šíře 4,5m
    * 2x MANFROTTO 126 BSU STAND plus ráhna


* Řízení
    * Stairville LED Commander 16/2
    * DMX Pipe


* Výrobníky
    * Hazer Stairville Hz-200

## Video technika (4k, HD a SD):

* Kamery
    * 2x Blackmagic Design Production Camera 4k
    * Canon XA 25
    * Panasonic AG-AC8
    * GoPro Hero 4 Black edition
    * 2x GoPro Hero 2


* Rigy a stabilizátory
    * DJI Ronin-M
    * Tilta Armor-man II ARM-T02

* LanParte Rig pro BMPC 4k


* Follow Focus
    * DMOVIE PDL-FZ Remote Live II Fingerwheel Controller Follow Focus Kit (Dual Channel)


* Bezdrátový přenos
    * Teradek Bolt 1000


* Objektivy
    * Canon 70-200 F/4 L
    * Canon 70-200 F/2,8 L IS II
    * Canon 300 F/4 L IS
    * Samyang 85mm T/1,5 VDSLR II pro Canon
    * Samyang 50mm T/1,5 VDSLR II pro Canon
    * Samyang 16mm T/2,2 VDSLR II pro Canon


* Střižny a kontrolery
    * Blackmagic Design ATEM 1M/E Production Studio 4K
    * Blackmagic Design ATEM 1M/E Broadcast Panel
    * Blackmagic Design ATEM Television Studio
    * Datavideo SE-500
    * Blackmagic Design DaVinci Resolve Mini Panel


* Video procesory
    * Blackmagic Design Teranex AV
    * Blackmagic Design Teranex Express


* Převodníky
    * Blackmagic Design H.264 Pro Recorder
    * Blackmagic Design Mini Converter HDMI to SDI
    * Blackmagic Design Micro Converter HDMI to SDI
    * Blackmagic Mini Converter SDI to Audio
    * 8x Datavideo DAC 70
    * 3x Datavideo DAC 60
    * Blackmagic Design Mini Converter SDI to HDMi 4k
    * Blackmagic Design Mini Converter SDI to Analog 4k
    * 2x Blackmagic Design Micro Converter SDI to HDMI
    * Blackmagic Design HDLink Pro DVI
    * 2x Blackmagic Design Mini Converter Optical Fiber 4k
    * 2x Blackmagic Design Mini Converter Optical Fiber


* Video karty
    * Blackmagic Design DeckLink 4k
    * Blackmagic Design DeckLink Duo 2
    * Blackmagic Design DeckLink Mini Recorder
    * Blackmagic Design Intensity Shuttle
    * Datavideo DAC 100


* Náhledové monitory
    * Blackmagic Design SmartView 4k
    * Blackmagic Design SmartView HD
    * Blackmagic Design SmartView Duo
    * Blackmagic Design Video Assist 4k
    * Blackmagic Design Video Assist

* HW záznam
    * Blackmagic Design HyperDeck Studio Pro 2

* Distribuce signálu
    * Blackmagic Design Smart Videohub 40x40
