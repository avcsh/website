#
# Provide ssh-agent and scp command in CD environment
#
FROM jekyll/jekyll:stable

RUN apk --update --no-cache add openssh
