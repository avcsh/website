# AVC.SH Website

# Contribution guide

By pushing to the branch `master` automated deployment to production server is triggered.
When you push to the branch `beta` changes will be visible at [beta.avc.sh](http://beta.avc.sh).

The used template is packed in repository as `transcend.zip`, original source is at [Colorlib](https://colorlib.com/wp/template/transcend/) website.

I'm trying to make the website multilingual from beginning thus before contributing please read [this guide](https://www.sylvaindurand.org/making-jekyll-multilingual/)
